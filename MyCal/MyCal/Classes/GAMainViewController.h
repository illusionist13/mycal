//
//  GAMainViewController.h
//  MyCal
//
//  Created by Gaurav on 5/5/16.
//  Copyright © 2016 Gaurav Mishra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GAMainViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIView *tileView;
@property (weak, nonatomic) IBOutlet UIView *eventsView;

@end
