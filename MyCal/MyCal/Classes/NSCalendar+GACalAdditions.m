//
//  NSCalendar+GACalAdditions.m
//  MyCal
//
//  Created by Gaurav on 5/5/16.
//  Copyright © 2016 Gaurav Mishra. All rights reserved.
//

#import "NSCalendar+GACalAdditions.h"

@implementation NSCalendar (GACalAdditions)
+ (NSCalendar*)calendarFromPreferenceString:(NSString*)string
{
    if ([string isEqualToString:@"gregorian"])
        return [[NSCalendar alloc]initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    else if ([string isEqualToString:@"buddhist"])
        return [[NSCalendar alloc]initWithCalendarIdentifier:NSCalendarIdentifierBuddhist];
    else if ([string isEqualToString:@"chinese"])
        return [[NSCalendar alloc]initWithCalendarIdentifier:NSCalendarIdentifierChinese];
    else if ([string isEqualToString:@"hebrew"])
        return [[NSCalendar alloc]initWithCalendarIdentifier:NSCalendarIdentifierHebrew];
    else if ([string isEqualToString:@"islamic"])
        return [[NSCalendar alloc]initWithCalendarIdentifier:NSCalendarIdentifierIslamic];
    else if ([string isEqualToString:@"islamicCivil"])
        return [[NSCalendar alloc]initWithCalendarIdentifier:NSCalendarIdentifierIslamicCivil];
    else if ([string isEqualToString:@"japanese"])
        return [[NSCalendar alloc]initWithCalendarIdentifier:NSCalendarIdentifierJapanese];
    else if ([string isEqualToString:@"republicOfChina"])
        return [[NSCalendar alloc]initWithCalendarIdentifier:NSCalendarIdentifierRepublicOfChina];
    else if ([string isEqualToString:@"persian"])
        return [[NSCalendar alloc]initWithCalendarIdentifier:NSCalendarIdentifierPersian];
    else if ([string isEqualToString:@"indian"])
        return [[NSCalendar alloc]initWithCalendarIdentifier:NSCalendarIdentifierIndian];
    else if ([string isEqualToString:@"iso8601"])
        return [[NSCalendar alloc]initWithCalendarIdentifier:NSCalendarIdentifierISO8601];
    return [NSCalendar currentCalendar];
}

- (NSDate*)gastartOfDayForDate:(NSDate*)date
{
    if (![self respondsToSelector:@selector(startOfDayForDate:)]) {
        // keep only day, month and year components
        NSDateComponents* comps = [self components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:date];
        return [self dateFromComponents:comps];
    }
    // startOfDayForDate: is only available in iOS 8 and later
    return [self startOfDayForDate:date];
}

- (NSDate*)nextStartOfDayForDate:(NSDate*)date
{
    NSDateComponents* comps = [NSDateComponents new];
    comps.day = 1;
    NSDate *next = [self dateByAddingComponents:comps toDate:date options:0];
    return [self gastartOfDayForDate:next];
}

- (NSDate*)startOfWeekForDate:(NSDate*)date
{
    NSDate *firstDay = nil;
    [self rangeOfUnit:NSCalendarUnitWeekOfMonth startDate:&firstDay interval:NULL forDate:date];
    return firstDay;
}

- (NSDate*)nextStartOfWeekForDate:(NSDate*)date
{
    NSDateComponents* comps = [NSDateComponents new];
    comps.day = 7;
    NSDate *next = [self dateByAddingComponents:comps toDate:date options:0];
    return [self startOfWeekForDate:next];
}

- (NSDate*)startOfMonthForDate:(NSDate*)date
{
    NSDate *firstDay = nil;
    [self rangeOfUnit:NSCalendarUnitMonth startDate:&firstDay interval:NULL forDate:date];
    return firstDay;
}

- (NSDate *)lastOfMonthForDate:(NSDate *)date{
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateComponents* comps = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSWeekCalendarUnit|NSWeekdayCalendarUnit fromDate:date]; // Get necessary date components
    
    // set last of month
    [comps setMonth:[comps month]+1];
    [comps setDay:0];
    NSDate *tDateMonth = [calendar dateFromComponents:comps];
    return tDateMonth;
}

- (NSDate*)nextStartOfMonthForDate:(NSDate*)date
{
    NSDate *firstDay = [self startOfMonthForDate:date];
    NSDateComponents *comp = [NSDateComponents new];
    comp.month = 1;
    return [self dateByAddingComponents:comp toDate:firstDay options:0];
}

- (NSDate*)startOfYearForDate:(NSDate*)date
{
    NSDate *firstDay = nil;
    [self rangeOfUnit:NSCalendarUnitYear startDate:&firstDay interval:NULL forDate:date];
    return firstDay;
}

// one-based index of the week in month for given date
- (NSUInteger)indexOfWeekInMonthForDate:(NSDate*)date
{
    // [NSCalendar ordinalityOfUnit:NSCalendarUnitWeekOfMonth inUnit:NSCalendarUnitMonth forDate:] is one-based
    // but the first week is considered only if its number of days is at least equal to the minimumDaysInFirstWeek value.
    
    NSUInteger index = [self ordinalityOfUnit:NSCalendarUnitWeekOfMonth inUnit:NSCalendarUnitMonth forDate:date];
    
    NSDate *startOfMonth = [self startOfMonthForDate:date];
    NSRange firstWeekRange = [self rangeOfUnit:NSCalendarUnitDay inUnit:NSCalendarUnitWeekOfMonth forDate:startOfMonth];
    
    if (firstWeekRange.length < self.minimumDaysInFirstWeek) {
        index++;
    }
    
    return index;
}

- (BOOL)isDate:(NSDate*)date1 sameDayAsDate:(NSDate*)date2
{
    if (!date1 || !date2)
        return NO;
    
    return ([[self gastartOfDayForDate:date1] compare:[self gastartOfDayForDate:date2]] == NSOrderedSame);
}

- (BOOL)isDate:(NSDate*)date1 sameMonthAsDate:(NSDate*)date2
{
    if (!date1 || !date2)
        return NO;
    
    NSDate* start1, *start2;
    [self rangeOfUnit:NSCalendarUnitMonth startDate:&start1 interval:nil forDate:date1];
    [self rangeOfUnit:NSCalendarUnitMonth startDate:&start2 interval:nil forDate:date2];
    
    return ([start1 compare:start2] == NSOrderedSame);
}

@end
