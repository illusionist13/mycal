//
//  GADateRange.h
//  MyCal
//
//  Created by Gaurav on 5/5/16.
//  Copyright © 2016 Gaurav Mishra. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GADateRange : NSObject

@property (nonatomic, copy) NSDate *start;      // start of the range
@property (nonatomic, copy) NSDate *end;        // end of the range (excluding)
@property (nonatomic, readonly) BOOL isEmpty;   // range is empty is start is equal to end

+ (instancetype)dateRangeWithStart:(NSDate*)start end:(NSDate*)end;
- (instancetype)initWithStart:(NSDate*)start end:(NSDate*)end;

- (BOOL)isEqualToDateRange:(GADateRange*)range;
- (NSDateComponents*)components:(NSCalendarUnit)unitFlags forCalendar:(NSCalendar*)calendar;
- (BOOL)containsDate:(NSDate*)date;
- (void)intersectDateRange:(GADateRange*)range;
- (BOOL)intersectsDateRange:(GADateRange*)range;
- (BOOL)includesDateRange:(GADateRange*)range;
- (void)unionDateRange:(GADateRange*)range;
- (void)enumerateDaysWithCalendar:(NSCalendar*)calendar usingBlock:(void (^)(NSDate *day, BOOL *stop))block;
@end
