//
//  NSCalendar+GACalAdditions.h
//  MyCal
//
//  Created by Gaurav on 5/5/16.
//  Copyright © 2016 Gaurav Mishra. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSCalendar (GACalAdditions)


+ (NSCalendar*)calendarFromPreferenceString:(NSString*)string;

- (NSDate*)gastartOfDayForDate:(NSDate*)date;
- (NSDate*)nextStartOfDayForDate:(NSDate*)date;
- (NSDate*)startOfWeekForDate:(NSDate*)date;
- (NSDate*)nextStartOfWeekForDate:(NSDate*)date;
- (NSDate*)startOfMonthForDate:(NSDate*)date;
- (NSDate*)nextStartOfMonthForDate:(NSDate*)date;
- (NSDate*)startOfYearForDate:(NSDate*)date;
- (NSUInteger)indexOfWeekInMonthForDate:(NSDate*)date;
- (NSDate *)lastOfMonthForDate:(NSDate *)date;

- (BOOL)isDate:(NSDate*)date1 sameDayAsDate:(NSDate*)date2;
- (BOOL)isDate:(NSDate*)date1 sameMonthAsDate:(NSDate*)date2;

@end
