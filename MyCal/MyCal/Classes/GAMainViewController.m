//
//  GAMainViewController.m
//  MyCal
//
//  Created by Gaurav on 5/5/16.
//  Copyright © 2016 Gaurav Mishra. All rights reserved.
//

#import "GAMainViewController.h"
#import "AppDelegate.h"

@interface GAMainViewController (){
    AppDelegate *appD;
}

@end

@implementation GAMainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    appD = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [self addChildViewController:appD.welcomePageVC];
    [self.tileView addSubview:appD.welcomePageVC.view];
    
    [self addChildViewController:appD.eventsPageVC];
    [self.eventsView addSubview:appD.eventsPageVC.view];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
