//
//  GADateCollectionViewCell.h
//  MyCal
//
//  Created by Gaurav on 5/4/16.
//  Copyright © 2016 Gaurav Mishra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GADateCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UIImageView *dateHighlighterImageView;
@property (weak, nonatomic) IBOutlet UILabel *monthLabel;

@end
