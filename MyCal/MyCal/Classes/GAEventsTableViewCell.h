//
//  GAEventsTableViewCell.h
//  MyCal
//
//  Created by Gaurav on 5/5/16.
//  Copyright © 2016 Gaurav Mishra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GAEventsTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleType;

@end
