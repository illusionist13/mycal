//
//  GADateCollectionViewController.m
//  MyCal
//
//  Created by Gaurav on 5/4/16.
//  Copyright © 2016 Gaurav Mishra. All rights reserved.
//

#import "GADateCollectionViewController.h"
#import "GADateRange.h"
#import "NSCalendar+GACalAdditions.h"
#import "GADateCollectionViewCell.h"
#import "AppDelegate.h"

@interface GADateCollectionViewController (){
    AppDelegate *appD;
}
@property (nonatomic, strong) NSCalendar *calendar;
@property (nonatomic, strong) NSDate *startDate;
@property (nonatomic, strong) GADateRange *dateRange;
@property (nonatomic, assign) NSUInteger numberOfLoadedMonths;
@property (nonatomic, strong) NSArray *weekDaysArray;
@property (nonatomic, assign) NSInteger counting;


@end

@implementation GADateCollectionViewController

static NSString * const reuseIdentifier = @"Cell";

- (void)viewDidLoad {
    [super viewDidLoad];
    self.numberOfLoadedMonths = 12;
    self.counting =0;

    appD = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    self.weekDaysArray = [NSArray arrayWithObjects:@"Sun", @"Mon", @"Tues", @"Wed", @"Thurs", @"Fri", @"Sat", nil];
    // Uncomment the following line to preserve selection between presentations
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Register cell classes
//    [self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:reuseIdentifier];
    
    // Do any additional setup after loading the view.
//    GADateRange *range = nil;

    self.calendar = [[NSCalendar alloc]initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
//    NSArray *visible = [[self.collectionView indexPathsForVisibleItems]sortedArrayUsingSelector:@selector(compare:)];
//    if (visible.count) {
//        NSDate *first = [self dateForDayAtIndexPath:[visible firstObject]];
//        NSDate *last = [self dateForDayAtIndexPath:[visible lastObject]];
//        
//        // end date of the range is excluded, so set it to next day
//        last = [self.calendar dateByAddingUnit:NSCalendarUnitDay value:1 toDate:last options:0];
//        
//        range = [GADateRange dateRangeWithStart:first end:last];
//    }
    
    [self.collectionView registerNib:[UINib nibWithNibName:@"GADateCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:reuseIdentifier];
    [self.collectionView reloadData];
}

-(void)viewDidAppear:(BOOL)animated{
    
    static NSDateFormatter *dateFormatter = nil;
    if (dateFormatter == nil) {
        dateFormatter = [NSDateFormatter new];
    }
    
    NSTimeZone* sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    NSTimeZone* destinationTimeZone = [NSTimeZone systemTimeZone];
    
    NSInteger sourceGMTOffset = [sourceTimeZone secondsFromGMTForDate:[NSDate date]];
    NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:[NSDate date]];
    NSTimeInterval interval = destinationGMTOffset - sourceGMTOffset;
    NSDate* destinationDate = [[NSDate alloc] initWithTimeInterval:interval sinceDate:[NSDate date]];

    NSDateComponents *requiredComponents = [self.calendar components:(NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit) fromDate:destinationDate];
    NSDateComponents *startComponents = [self.calendar components:(NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit) fromDate:self.startDate];
    
    NSNumber *weekDayStart = [self getWeekDayOfStartOfMonthAtSection:abs(requiredComponents.month - startComponents.month)];
    
    NSIndexPath *selectedIndexPath1 = [NSIndexPath indexPathForItem:(requiredComponents.day-1 + ([weekDayStart intValue]-1)) inSection:abs(requiredComponents.month - startComponents.month)];
    
    [appD.welcomePageVC.collectionView scrollToItemAtIndexPath:selectedIndexPath1 atScrollPosition:UICollectionViewScrollPositionTop animated:NO];
}
// public
- (void)setDateRange:(GADateRange*)dateRange
{
    // nil dateRange means 'inifinite' scrolling
    if (dateRange != _dateRange && ![dateRange isEqual:_dateRange]) {
        NSDate *firstDate = self.visibleDays.start;
        
        _dateRange = nil;
        
        if (dateRange) {
            
            // adjust start and end date of new range on month boundaries
            NSDate *start = [self.calendar startOfMonthForDate:dateRange.start];
            NSDate *end = [self.calendar startOfMonthForDate:dateRange.end];
            _dateRange = [GADateRange dateRangeWithStart:start end:end];
            
            // adjust startDate so that it falls inside new range
            if (![_dateRange includesDateRange:self.loadedDateRange]) {
                self.startDate = _dateRange.start;
            }
            
            if (![_dateRange containsDate:firstDate]) {
                firstDate = [NSDate date];
                if (![_dateRange containsDate:firstDate]) {
                    firstDate = _dateRange.start;
                }
            }
        }
        
        [self.collectionView reloadData];
        
//        [self scrollToDate:firstDate animated:NO];
    }
}


// range of loaded months
- (GADateRange*)loadedDateRange
{
    NSDateComponents *comps = [NSDateComponents new];
    comps.month = self.numberOfLoadedMonths;
    NSDate *endDate = [self.calendar dateByAddingComponents:comps toDate:self.startDate options:0];
    return [GADateRange dateRangeWithStart:self.startDate end:endDate];
}


// public
- (GADateRange*)visibleDays
{
    [self.collectionView layoutIfNeeded];
    
    GADateRange *range = nil;
    
    NSArray *visible = [[self.collectionView indexPathsForVisibleItems]sortedArrayUsingSelector:@selector(compare:)];
    if (visible.count) {
        NSDate *first = [self dateForDayAtIndexPath:[visible firstObject]];
        NSDate *last = [self dateForDayAtIndexPath:[visible lastObject]];
        
        // end date of the range is excluded, so set it to next day
        last = [self.calendar dateByAddingUnit:NSCalendarUnitDay value:1 toDate:last options:0];
        
        range = [GADateRange dateRangeWithStart:first end:last];
    }
    return range;
}

// indexPath is in the form (day, month)
- (NSDate*)dateForDayAtIndexPath:(NSIndexPath*)indexPath withWeekDayCount:(int)count
{
    NSDateComponents *comp = [NSDateComponents new];
    comp.month = indexPath.section;
    comp.day = indexPath.item-count+1;
    return [self.calendar dateByAddingComponents:comp toDate:self.startDate options:0];
}


// indexPath is in the form (day, month)
- (NSDate*)dateForDayAtIndexPath:(NSIndexPath*)indexPath
{
    NSDateComponents *comp = [NSDateComponents new];
    comp.month = indexPath.section;
    comp.day = indexPath.item;
    return [self.calendar dateByAddingComponents:comp toDate:self.startDate options:0];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSDate*)startDate
{
    _startDate = [self sixMonthBeforeDate:[NSDate date]];

    if (_startDate == nil) {
//        _startDate = [self.calendar startOfMonthForDate:[NSDate date]];
        
        if (self.dateRange && ![self.dateRange containsDate:_startDate]) {
            _startDate = self.dateRange.start;
        }
    }
    return _startDate;
}

- (NSDate *)sixMonthBeforeDate:(NSDate *)date{
    NSDateComponents *comps = [NSDateComponents new];
    comps.month = -6;
    NSDate *newDate = [self.calendar dateByAddingComponents:comps toDate:[NSDate date] options:0];
    NSDate *previousStartDate = [self.calendar startOfMonthForDate:newDate];
    return previousStartDate;
}

- (NSDate *)sixMonthAfterDate:(NSDate *)date{
    NSDateComponents *comps = [NSDateComponents new];
    comps.month = 6;
    NSDate *newDate = [self.calendar dateByAddingComponents:comps toDate:[NSDate date] options:0];
    NSDate *previousStartDate = [self.calendar lastOfMonthForDate:newDate];
    return previousStartDate;
}

- (NSNumber *)getWeekDayOfStartOfMonthAtSection:(NSInteger)section{
    NSDate *date = [self dateStartingMonthAtIndex:section];
    NSDateComponents *comps = [self.calendar components:NSWeekdayCalendarUnit fromDate:date];
    int weekday = [comps weekday];
    return [NSNumber numberWithInt:weekday];
}


- (NSUInteger)numberOfDaysForMonthAtIndex:(NSUInteger)month
{
    NSDate *date = [self dateStartingMonthAtIndex:month];
    NSRange range = [self.calendar rangeOfUnit:NSCalendarUnitDay inUnit:NSCalendarUnitMonth forDate:date];
    return range.length;
}
- (NSDate*)dateStartingMonthAtIndex:(NSUInteger)month
{
    return [self dateForDayAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:month]];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
#warning Incomplete implementation, return the number of sections
    NSLog(@"%lu \n", (unsigned long)self.numberOfLoadedMonths);

    return self.numberOfLoadedMonths;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
#warning Incomplete implementation, return the number of items

    NSLog(@"%lu \n", (unsigned long)[self numberOfDaysForMonthAtIndex:section]);
    NSNumber *weekDayStart = [self getWeekDayOfStartOfMonthAtSection:section];
    unsigned long numberOfDays = [self numberOfDaysForMonthAtIndex:section];
    
    return ([weekDayStart intValue]-1 + numberOfDays);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    GADateCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    NSNumber *weekDayStart = [self getWeekDayOfStartOfMonthAtSection:indexPath.section];
    for (int i=1; i<[weekDayStart intValue]; i++) {
        if (indexPath.item <=(i-1)) {
            cell.monthLabel.text = @"";
            cell.dateLabel.text = @"";
            cell.dateHighlighterImageView.hidden = YES;
            return cell;
        }
    }

    NSDate *date = [self dateForDayAtIndexPath:indexPath withWeekDayCount:[weekDayStart intValue]];
    static NSDateFormatter *dateFormatter = nil;
    if (dateFormatter == nil) {
        dateFormatter = [NSDateFormatter new];
    }
    
    NSTimeZone* sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    NSTimeZone* destinationTimeZone = [NSTimeZone systemTimeZone];
    
    NSInteger sourceGMTOffset = [sourceTimeZone secondsFromGMTForDate:date];
    NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:date];
    NSTimeInterval interval = destinationGMTOffset - sourceGMTOffset;
    
    NSDate* destinationDate = [[NSDate alloc] initWithTimeInterval:interval sinceDate:date];
    
    NSTimeZone* sourceTimeZone1 = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    NSTimeZone* destinationTimeZone1 = [NSTimeZone systemTimeZone];
    
    NSInteger sourceGMTOffset1 = [sourceTimeZone1 secondsFromGMTForDate:[NSDate date]];
    NSInteger destinationGMTOffset1 = [destinationTimeZone1 secondsFromGMTForDate:[NSDate date]];
    NSTimeInterval interval1 = destinationGMTOffset1 - sourceGMTOffset1;
    
    NSDate* destinationDate1 = [[NSDate alloc] initWithTimeInterval:interval1 sinceDate:[NSDate date]];
    
    dateFormatter.dateFormat = @"d";
    NSString *dayStr = [dateFormatter stringFromDate:destinationDate];
    
    if (dayStr.integerValue == 1) {
        dateFormatter.dateFormat = @"MMM-YY";
        NSString *str;
        str = [dateFormatter stringFromDate:destinationDate];
        cell.monthLabel.text = str;
        cell.monthLabel.hidden = NO;
    }
    else{
        cell.monthLabel.hidden = YES;
    }
    cell.dateHighlighterImageView.hidden = YES;

    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
    NSDateComponents *oldcomponents = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:self.startDate];


    if (self.selectedIndexPath == nil) {
        if ((indexPath.item == (components.day - [weekDayStart intValue]+1)) && indexPath.section == abs(components.month - oldcomponents.month)) {
            self.selectedIndexPath = indexPath;
            
            cell.dateHighlighterImageView.hidden = NO;
            cell.dateHighlighterImageView.layer.cornerRadius = 22;
            cell.dateHighlighterImageView.layer.masksToBounds = YES;
            
        }
    }
    
//    if ([self.calendar isDate:destinationDate sameDayAsDate:destinationDate1]) {
//        cell.dateHighlighterImageView.hidden = YES;
//        
//        if (self.selectedIndexPath == nil) {
//                self.selectedIndexPath = [NSIndexPath indexPathForItem:(indexPath.item+1) inSection:indexPath.section];
//            
//        }
//        
//
//    }
    
    
    cell.dateLabel.text = dayStr;
    // Configure the cell
    
    return cell;
}

#pragma mark <UICollectionViewDelegate>

/*
// Uncomment this method to specify if the specified item should be highlighted during tracking
- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
	return YES;
}
*/


// Uncomment this method to specify if the specified item should be selected
- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    GADateCollectionViewCell *cell = (GADateCollectionViewCell *)[collectionView cellForItemAtIndexPath:self.selectedIndexPath];
    cell.dateHighlighterImageView.hidden = YES;
    
    self.selectedIndexPath = indexPath;
    
    GADateCollectionViewCell *cell2 = (GADateCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    cell2.dateHighlighterImageView.hidden = YES;
    cell2.dateHighlighterImageView.layer.cornerRadius = 22;
    cell2.dateHighlighterImageView.layer.masksToBounds = YES;
    
    NSDate *selectedDate = [self dateForDayAtIndexPath:indexPath];
    NSDateComponents *selectedDateComponents = [self.calendar components:(NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit) fromDate:selectedDate];

    NSString *uniqueString = [NSString stringWithFormat:@"min%ld%ld", (long)selectedDateComponents.day, (long)selectedDateComponents.month];
    
    NSDictionary *dateDict = [appD.eventsPageVC.dateDict objectForKey:uniqueString];
    
    if (dateDict !=nil) {
        NSUInteger sectionNumber = [[dateDict objectForKey:@"sectionValue"] integerValue];
        NSIndexPath *eventsIndexPath = [NSIndexPath indexPathForRow:0 inSection:sectionNumber];
        [appD.eventsPageVC.tableView scrollToRowAtIndexPath:eventsIndexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
    
//    for (NSDictionary *dateDict in appD.eventsPageVC.dateDict) {
//        for (NSDate *date in [dateDict objectForKey:@"Date"]) {
//            NSDateComponents *dateComponents = [self.calendar components:(NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit) fromDate:date];
//
//            if ((selectedDateComponents.day == dateComponents.day) && (selectedDateComponents.month == dateComponents.month) && (selectedDateComponents.year == dateComponents.year)) {
//                NSIndexPath *eventsIndexPath = [NSIndexPath indexPathForRow:0 inSection:];
//                
//            }
//        }
//    }

    
    return YES;
}


/*
// Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
- (BOOL)collectionView:(UICollectionView *)collectionView shouldShowMenuForItemAtIndexPath:(NSIndexPath *)indexPath {
	return NO;
}

- (BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	return NO;
}

- (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	
}
*/

@end
