//
//  GAEventsTableViewController.h
//  MyCal
//
//  Created by Gaurav on 5/5/16.
//  Copyright © 2016 Gaurav Mishra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GAEventsTableViewController : UITableViewController{
    
}
@property (nonatomic, strong) NSMutableDictionary *dateDict;
@property (nonatomic, assign) NSUInteger totalNumberOfDays;


@end
