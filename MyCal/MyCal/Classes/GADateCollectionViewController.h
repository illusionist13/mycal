//
//  GADateCollectionViewController.h
//  MyCal
//
//  Created by Gaurav on 5/4/16.
//  Copyright © 2016 Gaurav Mishra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GADateCollectionViewController : UICollectionViewController
{
    
}
@property (nonatomic, strong) NSIndexPath *selectedIndexPath;

@end
