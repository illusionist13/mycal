//
//  GAEventsTableViewController.m
//  MyCal
//
//  Created by Gaurav on 5/5/16.
//  Copyright © 2016 Gaurav Mishra. All rights reserved.
//

#import "GAEventsTableViewController.h"
#import "GADateRange.h"
#import "NSCalendar+GACalAdditions.h"
#import <EventKit/EventKit.h>
#import "AppDelegate.h"
#import "GAEventsTableViewCell.h"
#import "GADateCollectionViewCell.h"

@interface GAEventsTableViewController (){
    AppDelegate *appD;
}
@property (nonatomic, strong) NSCalendar *calendar;
@property (nonatomic, strong) NSDate *startDate;
@property (nonatomic, strong) GADateRange *dateRange;
@property (nonatomic, assign) NSUInteger numberOfLoadedMonths;
@property (nonatomic, strong) NSArray *eventsList;
@property (nonatomic, strong) NSIndexPath *ongoingMonthIndexPath;
@property (nonatomic, assign) NSInteger rowCount;


@end

@implementation GAEventsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    appD = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    self.calendar = [[NSCalendar alloc]initWithCalendarIdentifier:NSCalendarIdentifierGregorian];

    self.dateDict = [[NSMutableDictionary alloc] init];
    self.numberOfLoadedMonths = 9;
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    self.totalNumberOfDays = 0;
    
    [self.tableView registerNib:[UINib nibWithNibName:@"GAEventsTableViewCell" bundle:nil] forCellReuseIdentifier:@"kEventsCell"];

    
    self.eventsList = [self getAllEvents];
    for (int i = 0; i< self.numberOfLoadedMonths; i++) {
        NSUInteger numberOfDays = [self numberOfDaysForMonthAtIndex:i];
        for (int j = 1; j<=numberOfDays; j++) {
            NSDateComponents *dateComponents = [NSDateComponents new];
            dateComponents.day = j;
            dateComponents.month = i;
            NSDate *newDate = [self.calendar dateByAddingComponents:dateComponents toDate:self.startDate options:0];
            NSMutableDictionary *dataDict = [[NSMutableDictionary alloc] initWithObjectsAndKeys:newDate, @"Date", nil];
            [self.dateDict setObject:dataDict forKey:[NSString stringWithFormat:@"%lu", (unsigned long)self.totalNumberOfDays]];
            
            NSMutableDictionary *delegateDict = [[NSMutableDictionary alloc] initWithObjectsAndKeys:newDate, @"DelDate", [NSString stringWithFormat:@"%lu", (unsigned long)self.totalNumberOfDays], @"sectionValue" , nil];
            
            NSDateComponents *newDateComponents = [self.calendar components:(NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit) fromDate:newDate];

            NSString *uniqueString = [NSString stringWithFormat:@"min%ld%ld", (long)newDateComponents.day, (long)newDateComponents.month];
            [self.dateDict setObject:delegateDict forKey:uniqueString];
            
            self.totalNumberOfDays = self.totalNumberOfDays+1;

        }
    }
    
    
}

-(void)viewDidAppear:(BOOL)animated{
    NSDate *selectedDate = [NSDate date];
    NSDateComponents *selectedDateComponents = [self.calendar components:(NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit) fromDate:selectedDate];
    
    NSString *uniqueString = [NSString stringWithFormat:@"min%ld%ld", (long)selectedDateComponents.day, (long)selectedDateComponents.month];
    
    NSDictionary *dateDict = [appD.eventsPageVC.dateDict objectForKey:uniqueString];
    
    if (dateDict !=nil) {
        NSUInteger sectionNumber = [[dateDict objectForKey:@"sectionValue"] integerValue];
        NSIndexPath *eventsIndexPath = [NSIndexPath indexPathForRow:0 inSection:sectionNumber];
        [appD.eventsPageVC.tableView scrollToRowAtIndexPath:eventsIndexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }

}


- (NSArray *)getAllEvents{
    
    NSTimeZone* sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    NSTimeZone* destinationTimeZone = [NSTimeZone systemTimeZone];
    
    NSInteger sourceGMTOffset = [sourceTimeZone secondsFromGMTForDate:[NSDate date]];
    NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:[NSDate date]];
    NSTimeInterval interval = destinationGMTOffset - sourceGMTOffset;
    
    NSDate* destinationDate = [[NSDate alloc] initWithTimeInterval:interval sinceDate:[NSDate date]];
    
    
    
    NSInteger sourceGMTOffset1 = [sourceTimeZone secondsFromGMTForDate:[NSDate date]];
    NSInteger destinationGMTOffset1 = [destinationTimeZone secondsFromGMTForDate:[NSDate date]];
    NSTimeInterval interval1 = destinationGMTOffset1 - sourceGMTOffset1;
    
    NSDate* destinationDate1 = [[NSDate alloc] initWithTimeInterval:interval1 sinceDate:[self.calendar startOfMonthForDate:destinationDate]];
    NSDate *start = destinationDate1;

    NSDate *finish = [NSDate distantFuture];
    EKEventStore *eventStore = [[EKEventStore alloc] init];

    // use Dictionary for remove duplicates produced by events covered more one year segment
//    NSMutableDictionary *eventsDict = [NSMutableDictionary dictionaryWithCapacity:1024];
//    
//    NSDate* currentStart = [NSDate dateWithTimeInterval:0 sinceDate:start];
//    
//    int seconds_in_year = 60*60*24*365;
   NSPredicate *datePredicate;

    AppDelegate *appD = [[UIApplication sharedApplication] delegate];
    // code here for when the user allows your app to access the calendar
    datePredicate = [appD.eventStore predicateForEventsWithStartDate:start endDate:finish calendars:nil];


//    // enumerate events by one year segment because iOS do not support predicate longer than 4 year !
//    while ([currentStart compare:finish] == NSOrderedAscending) {
//        
//        NSDate* currentFinish = [NSDate dateWithTimeInterval:seconds_in_year sinceDate:currentStart];
//        
//        if ([currentFinish compare:finish] == NSOrderedDescending) {
//            currentFinish = [NSDate dateWithTimeInterval:0 sinceDate:finish];
//        }
//        NSPredicate *predicate = [eventStore predicateForEventsWithStartDate:currentStart endDate:currentFinish calendars:nil];
//        [eventStore enumerateEventsMatchingPredicate:predicate
//                                          usingBlock:^(EKEvent *event, BOOL *stop) {
//                                              
//                                              if (event) {
//                                                  [eventsDict setObject:event forKey:event.eventIdentifier];
//                                              }
//                                              
//                                          }];       
//        currentStart = [NSDate dateWithTimeInterval:(seconds_in_year + 1) sinceDate:currentStart];
//        
//    }
    NSArray *events = [eventStore eventsMatchingPredicate:datePredicate];

//    NSArray *events = [eventsDict allValues];
    return events;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
#warning Incomplete implementation, return the number of sections
    NSLog(@"%lu \n", (unsigned long)self.totalNumberOfDays);
    
    return self.totalNumberOfDays;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
#warning Incomplete implementation, return the number of rows
    NSLog(@"%lu \n", (unsigned long)[self numberOfDaysForMonthAtIndex:section]);
    NSDictionary *dataDict = [self.dateDict objectForKey:[NSString stringWithFormat:@"%lu", (unsigned long)section]];
    NSDate *requiredDate = [dataDict objectForKey:@"Date"];
    NSDateComponents *requiredComponents = [self.calendar components:(NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit) fromDate:requiredDate];

    NSUInteger count =0;
    for (int i=0; i< self.eventsList.count; i++) {
        EKEvent *eventDict = [self.eventsList objectAtIndex:i];
        NSDate *eventStartDate = eventDict.startDate;
        NSDate *eventEndDate = eventDict.endDate;
        
        NSDateComponents *startComponents = [self.calendar components:(NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit) fromDate:eventStartDate];
        NSDateComponents *endComponents = [self.calendar components:(NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit) fromDate:eventEndDate];

        
        if (((startComponents.day == requiredComponents.day) && (startComponents.month == requiredComponents.month) && (startComponents.year == requiredComponents.year)) ||  ((endComponents.day == requiredComponents.day) && (endComponents.month == requiredComponents.month) && (endComponents.year == requiredComponents.year)) ) {
            [dataDict setValue:eventDict forKey:[NSString stringWithFormat:@"event %lu", (unsigned long)count]];
            count++;
        }
    }
    if (count==0) {
        count++;
    }
//    return [self numberOfDaysForMonthAtIndex:section];
    return count;

}
- (nullable NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{

    NSDictionary *dateDict = [self.dateDict objectForKey:[NSString stringWithFormat:@"%ld", (long)section]];
    NSDate *sectionDate = [dateDict objectForKey:@"Date"];
    
    static NSDateFormatter *dateFormatter = nil;
    if (dateFormatter == nil) {
        dateFormatter = [NSDateFormatter new];
    }
    dateFormatter.dateFormat = @"d";
    NSString *dayStr = [dateFormatter stringFromDate:sectionDate];
    
    dateFormatter.dateFormat = @"MMM";
    NSString *str;
    str = [dateFormatter stringFromDate:sectionDate];

    NSString *sectionString =[NSString stringWithFormat:@"%@ %@", dayStr, str];
    return sectionString;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *dataDict = [self.dateDict objectForKey:[NSString stringWithFormat:@"%lu", (unsigned long)indexPath.section]];

    EKEvent *eventDict = [dataDict objectForKey:[NSString stringWithFormat:@"event %ld", indexPath.row]];
    
    

    GAEventsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"kEventsCell" forIndexPath:indexPath];
    
    if (eventDict == nil) {
        cell.titleType.text = @"No Event";
    }
    else{
        cell.titleType.text = eventDict.title;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didEndDisplayingHeaderView:(UIView *)view forSection:(NSInteger)section{

       NSDictionary *dataDict = [self.dateDict objectForKey:[NSString stringWithFormat:@"%ld", (long)section]];
    NSDate *date = [dataDict objectForKey:@"Date"];
    static NSDateFormatter *dateFormatter = nil;
    if (dateFormatter == nil) {
        dateFormatter = [NSDateFormatter new];
    }
    
    NSTimeZone* sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    NSTimeZone* destinationTimeZone = [NSTimeZone systemTimeZone];
    
    NSInteger sourceGMTOffset = [sourceTimeZone secondsFromGMTForDate:date];
    NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:date];
    NSTimeInterval interval = destinationGMTOffset - sourceGMTOffset;
    
    NSDate* destinationDate = [[NSDate alloc] initWithTimeInterval:interval sinceDate:date];
    
    
    NSDateComponents *requiredComponents;
    NSDateComponents *startComponents = [self.calendar components:(NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit) fromDate:self.startDate];

//    [self.calendar components:(NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit) fromDate:self.startDate toDate:date options:NSCalendarWrapComponents]
    NSNumber *weekDayStart;
    NSIndexPath *selectedIndexPath;
    
    NSInteger totalItems = [appD.welcomePageVC.collectionView numberOfItemsInSection:self.ongoingMonthIndexPath.section];
    // How many items are there per row?
    NSInteger currItem;
    CGFloat currRowOriginY = CGFLOAT_MAX;
    for (currItem = 0; currItem < totalItems; currItem++) {
        UICollectionViewLayoutAttributes *attributes =
        [appD.welcomePageVC.collectionView.collectionViewLayout layoutAttributesForItemAtIndexPath:
         [NSIndexPath indexPathForItem:currItem inSection:self.ongoingMonthIndexPath.section]];
        
        if (currItem == 0) {
            currRowOriginY = attributes.frame.origin.y;
            continue;
        }
        
        if (attributes.frame.origin.y > currRowOriginY + 44.0f) {
            break;
        }
    }

    NSLog(@"new row started at item %ld", (long)currItem);
    NSInteger totalRows = totalItems / currItem;
    if ((totalItems % currItem) >0) {
        totalRows++;
    }
    NSLog(@"%ld rows", (long)totalRows);
    

    
    if ([self isDayLastDayOfMonth:destinationDate]) {
        NSDate *dDate = [[NSDate alloc] initWithTimeInterval:interval sinceDate:[self.calendar nextStartOfMonthForDate:destinationDate]];
        weekDayStart = [self getWeekDayOfStartOfMonthAtDate:dDate];
        requiredComponents = [self.calendar components:(NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit) fromDate:dDate];

        selectedIndexPath = [NSIndexPath indexPathForItem:(requiredComponents.day+([weekDayStart intValue]-2)) inSection:abs(requiredComponents.month + (12-startComponents.month))];
        self.ongoingMonthIndexPath = selectedIndexPath;
        self.rowCount = 0;
    }
    else{
        weekDayStart = [self getWeekDayOfStartOfMonthAtDate:destinationDate];
        requiredComponents = [self.calendar components:(NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit) fromDate:destinationDate];

//        selectedIndexPath = [NSIndexPath indexPathForItem:(abs(requiredComponents.day-([weekDayStart intValue]-1))) inSection:abs(requiredComponents.month - startComponents.month)];
        
        
        if ([weekDayStart intValue] < requiredComponents.day) {
//            selectedIndexPath = [NSIndexPath indexPathForItem:(7*self.rowCount+(requiredComponents.day - ([weekDayStart intValue]+1))) inSection:self.ongoingMonthIndexPath.section];
//            selectedIndexPath = [NSIndexPath indexPathForItem:(abs((requiredComponents.day+([weekDayStart intValue]-1))%currItem)+7*self.rowCount) inSection:self.ongoingMonthIndexPath.section];

            
            selectedIndexPath = [NSIndexPath indexPathForItem:(abs(requiredComponents.day-([weekDayStart intValue]-1))) inSection:self.ongoingMonthIndexPath.section];
        }
        else{
            selectedIndexPath = [NSIndexPath indexPathForItem:(abs(requiredComponents.day+([weekDayStart intValue]-1))) inSection:self.ongoingMonthIndexPath.section];

        }
//        if ((requiredComponents.day+([weekDayStart intValue]-1))%currItem == 0) {
//            self.rowCount++;
//        }



    }
    

  
    [appD.welcomePageVC.collectionView scrollToItemAtIndexPath:selectedIndexPath atScrollPosition:UICollectionViewScrollPositionTop animated:YES];
    GADateCollectionViewCell *cell = (GADateCollectionViewCell *)[appD.welcomePageVC.collectionView cellForItemAtIndexPath:appD.welcomePageVC.selectedIndexPath];
    cell.dateHighlighterImageView.hidden = YES;
    
    appD.welcomePageVC.selectedIndexPath = selectedIndexPath;
    
    GADateCollectionViewCell *cell2 = (GADateCollectionViewCell *)[appD.welcomePageVC.collectionView cellForItemAtIndexPath:selectedIndexPath];
    cell2.dateHighlighterImageView.hidden = NO;
    cell2.dateHighlighterImageView.layer.cornerRadius = 22;
    cell2.dateHighlighterImageView.layer.masksToBounds = YES;
}
- (NSDate *)sixMonthBeforeDate:(NSDate *)date{
    NSDateComponents *comps = [NSDateComponents new];
    comps.month = -6;
    NSDate *newDate = [self.calendar dateByAddingComponents:comps toDate:[NSDate date] options:0];
    NSDate *previousStartDate = [self.calendar startOfMonthForDate:newDate];
    return previousStartDate;
}

- (BOOL)isDayLastDayOfMonth:(NSDate *)date{
    NSDateComponents *requiredComponents = [self.calendar components:(NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit) fromDate:date];
    
    NSRange range = [self.calendar rangeOfUnit:NSDayCalendarUnit inUnit:NSMonthCalendarUnit forDate:date];
    NSUInteger numberOfDaysInMonth = range.length;
    
    if (requiredComponents.day == numberOfDaysInMonth) {
        return YES;
    }
    else{
        return NO;
    }
    
}

- (BOOL)isFirstDayOfMonth:(NSDate *)date{
    
    NSDateComponents *components = [self.calendar components:(NSDayCalendarUnit | NSYearCalendarUnit | NSMonthCalendarUnit) fromDate:date];
    if (components.day == 1) {
        return YES;
    }
    else{
        return NO;
    }
}

- (NSDate *)sixMonthAfterDate:(NSDate *)date{
    NSDateComponents *comps = [NSDateComponents new];
    comps.month = 6;
    NSDate *newDate = [self.calendar dateByAddingComponents:comps toDate:[NSDate date] options:0];
    NSDate *previousStartDate = [self.calendar lastOfMonthForDate:newDate];
    return previousStartDate;
}

- (NSNumber *)getWeekDayOfStartOfMonthAtDate:(NSDate *)requiredDate{
    NSDate *date = [self.calendar startOfMonthForDate:requiredDate];
    NSDateComponents *comps = [self.calendar components:NSWeekdayCalendarUnit fromDate:date];
    int weekday = [comps weekday];
    return [NSNumber numberWithInt:weekday];
}

- (NSNumber *)getWeekDayOfStartOfMonthAtSection:(NSInteger)section{
    NSDate *date = [self dateStartingMonthAtIndex:section];
    NSDateComponents *comps = [self.calendar components:NSWeekdayCalendarUnit fromDate:date];
    int weekday = [comps weekday];
    return [NSNumber numberWithInt:weekday];
}
//- (void)scrollToRowAtIndexPath:(NSIndexPath *)indexPath atScrollPosition:(UITableViewScrollPosition)scrollPosition animated:(BOOL)animated{
//    
//}
//- (void)scrollViewDidScroll:(UITableView *)tableView{
//    
//}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Table view delegate

// In a xib-based application, navigation from a table can be handled in -tableView:didSelectRowAtIndexPath:
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Navigation logic may go here, for example:
    // Create the next view controller.
    <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:<#@"Nib name"#> bundle:nil];
    
    // Pass the selected object to the new view controller.
    
    // Push the view controller.
    [self.navigationController pushViewController:detailViewController animated:YES];
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

// public
- (void)setDateRange:(GADateRange*)dateRange
{
    // nil dateRange means 'inifinite' scrolling
    if (dateRange != _dateRange && ![dateRange isEqual:_dateRange]) {
        NSDate *firstDate = self.visibleDays.start;
        
        _dateRange = nil;
        
        if (dateRange) {
            
            // adjust start and end date of new range on month boundaries
            NSDate *start = [self.calendar startOfMonthForDate:dateRange.start];
            NSDate *end = [self.calendar startOfMonthForDate:dateRange.end];
            _dateRange = [GADateRange dateRangeWithStart:start end:end];
            
            // adjust startDate so that it falls inside new range
            if (![_dateRange includesDateRange:self.loadedDateRange]) {
                self.startDate = _dateRange.start;
            }
            
            if (![_dateRange containsDate:firstDate]) {
                firstDate = [NSDate date];
                if (![_dateRange containsDate:firstDate]) {
                    firstDate = _dateRange.start;
                }
            }
        }
        
        [self.tableView reloadData];
        
        //        [self scrollToDate:firstDate animated:NO];
    }
}


// range of loaded months
- (GADateRange*)loadedDateRange
{
    NSDateComponents *comps = [NSDateComponents new];
    comps.month = self.numberOfLoadedMonths;
    NSDate *endDate = [self.calendar dateByAddingComponents:comps toDate:self.startDate options:0];
    return [GADateRange dateRangeWithStart:self.startDate end:endDate];
}


// public
- (GADateRange*)visibleDays
{
    [self.tableView layoutIfNeeded];
    
    GADateRange *range = nil;
    
    NSArray *visible = [[self.tableView indexPathsForVisibleRows]sortedArrayUsingSelector:@selector(compare:)];
    if (visible.count) {
        NSDate *first = [self dateForDayAtIndexPath:[visible firstObject]];
        NSDate *last = [self dateForDayAtIndexPath:[visible lastObject]];
        
        // end date of the range is excluded, so set it to next day
        last = [self.calendar dateByAddingUnit:NSCalendarUnitDay value:1 toDate:last options:0];
        
        range = [GADateRange dateRangeWithStart:first end:last];
    }
    return range;
}


// indexPath is in the form (day, month)
- (NSDate*)dateForDayAtIndexPath:(NSIndexPath*)indexPath
{
    NSDateComponents *comp = [NSDateComponents new];
    comp.month = indexPath.section;
    comp.day = indexPath.item;
    return [self.calendar dateByAddingComponents:comp toDate:self.startDate options:0];
}


- (NSDate*)startDate
{
    _startDate = [self sixMonthBeforeDate:[NSDate date]];

    if (_startDate == nil) {
//        _startDate = [self.calendar startOfMonthForDate:[NSDate date]];
        
        if (self.dateRange && ![self.dateRange containsDate:_startDate]) {
            _startDate = self.dateRange.start;
        }
    }
    return _startDate;
}


- (NSUInteger)numberOfDaysForMonthAtIndex:(NSUInteger)month
{
    NSDate *date = [self dateStartingMonthAtIndex:month];
    NSRange range = [self.calendar rangeOfUnit:NSCalendarUnitDay inUnit:NSCalendarUnitMonth forDate:date];
    return range.length;
}
- (NSDate*)dateStartingMonthAtIndex:(NSUInteger)month
{
    return [self dateForDayAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:month]];
}




@end
