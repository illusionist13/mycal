//
//  AppDelegate.h
//  MyCal
//
//  Created by Gaurav on 5/4/16.
//  Copyright © 2016 Gaurav Mishra. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import <EventKit/EventKit.h>
#import "GADateCollectionViewController.h"
#import "GAEventsTableViewController.h"


@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, strong) GADateCollectionViewController *welcomePageVC;
@property (nonatomic, strong) GAEventsTableViewController *eventsPageVC;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (nonatomic, strong) UINavigationController *navController;
@property (nonatomic, strong) EKEventStore *eventStore;


- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;


@end

